# Usage
To install, use `stow vim`, etc, from the top level directory.

Creates symlinks such as: `~/.vimrc -> ~/dotfiles/vim/.vimrc`.

Reference: http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html

# Icons
Put in ~/.icons
`git clone https://github.com/horst3180/arc-icon-theme temp`

# gtk themes
Put in ~/.themes
`git clone https://github.com/addy-dclxvi/gtk-theme-collections ~/temp`

# xfwm themes
Put in ~/.themes
`git clone https://github.com/addy-dclxvi/Xfwm4-Theme-Collections ~/.themes`

# Fonts
```
# clone
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts
```
Grab from https://github.com/powerline/fonts
