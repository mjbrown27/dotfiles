" General Settings
set nocompatible            " not compatible with vi, not sure if necessary
set t_Co=256                " 256 colors
set number                  " line numbers
set hidden                  " buffers stay open in background
set scrolloff=8             " always keep 8 lines above or below cursor
set wildmenu
set wildmode=longest,full
set wildignorecase
set incsearch               " show results as they are typed
"set hlsearch               " highlight resuts, use :noh to temp turn off
set showcmd                 " show incomplete commands
set showmode                " show current mode
set ruler                   " show cursor position
set background=dark
set autoread                " automatically reload if file changes
set nostartofline           " save column pos when switching buffers
set directory=~/.vim/swapfiles//

hi MatchParen cterm=underline ctermbg=none  " highlight matching paren with underline
syntax on
filetype indent plugin on   " default settings below, superceded in .vim/ftplugin/*.vim
set tabstop=8
set expandtab
set shiftwidth=4
set softtabstop=4

" this may only work if vim is compiled with xterm_clipboard support, check with `vim --version`
"set clipboard=unnamedplus
"set clipboard=unnamed

" Install Vim-Plug
if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'edkolev/tmuxline.vim'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'tpope/vim-vinegar'
Plug 'arcticicestudio/nord-vim'
call plug#end()

"colorscheme nord

" vim-airline
set laststatus=2                                    " always show status line
"let g:airline_theme='base16_bright'
"let g:airline_theme='luna'
let g:airline_theme='bubblegum'
"let g:airline_theme='nord'
let g:airline_powerline_fonts = 1                   " powerline fonts
let g:airline#extensions#tabline#enabled = 1        " enable the list of buffers
let g:airline#extensions#tabline#fnamemod = ':t'    " show just the filename
let g:airline#extensions#tabline#tab_nr_type = 0    " tab number
let g:airline#extensions#tabline#show_tab_nr = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
"let g:airline#extensions#tabline#buffer_nr_show = 1 " show buffer number
"let g:airline#extensions#tmuxline#enabled = 0

" tmuxline
let g:tmuxline_preset = {
    \'a'    : '#S',
    \'cwin' : ['#I', '#W'],
    \'win'  : ['#I', '#W'],
    \'x'    : '#{?client_prefix,Prefix,      }',
    \'y'    : ['%I:%M%p', '%d-%b-%y'],
    \'z'    : '#h'}

" Mappings
imap jj <Esc>
nnoremap j gj
nnoremap k gk
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>

" Autocomds
" after any filetype plugin, remove these flags from format options to stop comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Read from project specific .vimrc files
set exrc
" Prevent :autocmd, shell, and write commands from project specific .vimrc files
set secure
